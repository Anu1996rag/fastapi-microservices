import pytest
from fastapi import status
from fastapi.testclient import TestClient
from inventory.main import app

client = TestClient(app)


@pytest.mark.parametrize("prod_id", ["01GJ5HD446S7104SBMY1JWCJF7"])
def test_get_product_details_success(inventory, prod_id):
    response = inventory.get_product(prod_id)
    assert response == status.HTTP_200_OK


def test_create_product(inventory, product):
    response = inventory.add_product(product)
    assert response == status.HTTP_201_CREATED


@pytest.mark.parametrize("prod_id", ["0erCJF7"])
def test_get_product_details_failure(prod_id):
    response = client.get(f'/products/{prod_id}')
    assert response.status_code == status.HTTP_404_NOT_FOUND


def test_get_all_products(inventory):
    response = inventory.get_all_products()
    assert response == status.HTTP_200_OK


@pytest.mark.parametrize("prod_id", ["01GJ5HD446S7104SBMY1JWCJF7"])
def test_delete_product(inventory, prod_id):
    result = inventory.delete_product(prod_id)
    assert result == status.HTTP_204_NO_CONTENT
