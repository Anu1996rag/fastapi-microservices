from typing import Optional

import pytest
from fastapi import status
from pydantic import BaseModel


class TestProductModel(BaseModel):
    id: Optional[str]
    name: str
    price: float
    quantity: int


class TestCreateOrderModel(BaseModel):
    id: Optional[str]
    quantity: int


@pytest.fixture
def product():
    return TestProductModel(
        name="test",
        price=100.00,
        quantity=10
    )


@pytest.fixture
def order():
    return TestCreateOrderModel(
        id="01GJ5HD446S7104SBMY1JWCJF7",
        quantity=100
    )


class InMemoryInventory:
    products = []

    def __init__(self):
        product = TestProductModel(
            name="test",
            price=100.00,
            quantity=10
        )
        product.id = "01GJ5HD446S7104SBMY1JWCJF7"
        InMemoryInventory.products.append(product.dict())

    def add_product(self, product: TestProductModel):
        InMemoryInventory.products.append(product.dict())
        return status.HTTP_201_CREATED

    def get_product(self, id):
        for product in InMemoryInventory.products:
            if product.get('id') == id:
                return status.HTTP_200_OK
        return status.HTTP_404_NOT_FOUND

    def get_all_products(self):
        if InMemoryInventory.products:
            return status.HTTP_200_OK
        return status.HTTP_404_NOT_FOUND

    def delete_product(self, id):
        for product in InMemoryInventory.products:
            if product.get('id') == id:
                InMemoryInventory.products.remove(product)
                return status.HTTP_204_NO_CONTENT
        return status.HTTP_403_FORBIDDEN


@pytest.fixture
def inventory():
    return InMemoryInventory()


class InMemoryOrders:
    orders = []

    def __init__(self):
        order = TestCreateOrderModel(
            id="01GJCE4PV8P718J8BER17MW7CH",
            quantity=100
        )
        InMemoryOrders.orders.append(order.dict())

    def get_order_details(self, id):
        for order in InMemoryOrders.orders:
            if order.get("id") == id:
                return status.HTTP_200_OK
        return status.HTTP_404_NOT_FOUND

    def add_order(self, order: TestCreateOrderModel):
        self.orders.append(order.dict())
        return status.HTTP_201_CREATED

    def get_all_orders(self):
        if InMemoryOrders.orders:
            return status.HTTP_200_OK
        return status.HTTP_404_NOT_FOUND


@pytest.fixture
def orders():
    return InMemoryOrders()
